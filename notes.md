Define API
- it means front-end makes request (POST,GET,DELETE,PATCH)to back-end
- then back-end gives response acc to the request - this is called making API.


STEP1 (installation)
npm init -y (for package .json)   
Open package.json 
1. Then below main: index.js => "type":"module"
- IT MEANS now .js supports import export  
2. Remove type below the scripts and write this instead "start":"nodemon index.js".


STEP2 (Make BACKEND application using EXPRESS and Attach the PORT)
1. for making express application. Download express (A)
2. npm i express (for downloading express)
3. npm i MONGOOSE (to connect to database)
inside the file of connectToMongoDB
 import mongoose from "mongoose"

export let connectToMongodb = () => {
    mongoose.connect("mongodb://0.0.0.0:27017")
}


STEP3 (import express in index.js) (A)
- import express from "express"
let expressApp = express() 
For attaching the port 
- expressApp.listen(8000,()=>{
console.log("app is listening at post 8000")
})
- import express from "express"
let expressApp = express()
expressApp.listen(8000,()=>{
console.log("application is listening at post 8000")
})

For requesting (THESE 2 ARE NEEDED)
- url:"localhost:8000"
- method:"get" or "post" or "patch" or "delete"

STEP 4 
- make folder src 
- then make another folder route

TO MAKE API WE HAVE TO 2 STEPS
1. Make and Define Router
2. Use that router to the main value(index.js)
i.e,By Writing expressApp.use(firstRouter)

IMP
 - if there is . in import gotta write .js extension in the end.
 - first ko ("/") means localhost:8000

REFERENCE 
firstRouter
  .route("/")
  .get(() => {
    console.log("i am age GET");
  })
  .post(() => {
    console.log("i am age POST");
  })
  .patch(() => {
    console.log("i am age PATCH");
  })
  .delete(() => {
    console.log("i am age DELETE");
  });

  Note: URL made up of two things route and query 
  WE CAN SEND DATA FROM POSTMAN IN THREE WAYS:
  1. request 
  2. body 
  3. params 
  
  NOTES: whatever you pass in a query (?) it becomes string automatically.
  For one request we must have one response.

  MIDDLEWARE - are the functions which has (req,res,next)
  next() is used for triggering the next middleware
  1st way
  - route middleware - route related
  - application middleware - main file means index.js

  2nd way
- normal middleware =>(req,res,next) => to trigger middleware => next()
- error middleware =>(err,req,res,next)  => to trigger error middleware => 
let error = new Error("my error")
      next(error)
      define object => schema
      define array => model needs name and object

CRUD is always related to database.

params => id
data => body

1. schema
2. model
3. Controller
4. Router
5. Index

IMAGE
- PACKAGES to install
- npm i multer
1. make public folder static so that it can be get through link adn open in browser.
- how to make it static ? Here,
add ons :expressApp.use(express.static("./public"));

the sum up : 
let expressApp = express();
expressApp.use(express.static("./public"));
expressApp.use(json());
connectToMongodb();

STEPS FOR IMAGE 
- send file from postman
- save file to our server (public folder)
- postman then sends link on console

GIT
local server ---------------->   GIT ----------------> GIT LAB 
             git add.                 git push origin
             git commit -m            branchName
             "message"
             -"add repo to gitlab"


