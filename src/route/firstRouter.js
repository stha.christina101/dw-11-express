import { Router } from "express";

export let firstRouter = Router();

firstRouter
  .route("/")
  .post(
    (req, res, next) => {
      console.log("i am middleware");
      req.name = "christina";
      next();
    },
    (req, res, next) => {
      console.log("i am middleware 2");
    }
  )
  .get(
    (req, res, next) => {
      console.log("i am middleware 1");
      let error = new Error("my error");
      next(error);
    },
    (err, req, res, next) => {
      console.log(err.message);
      console.log(" i am error middleware");
      next();
    },
    (req, res, next) => {
      console.log("i am middleware 2");
    }
  )

  .patch((req, res, next) => {
    let data = req.body;
    console.log(data);
    res.json(data);
  })
  .delete((req, res, next) => {
    res.json({
      success: true,
      message: "DELETED successfully",
    });
  });
