import { Router } from "express";
import {
  createUser,
  deleteUser,
  readSpecificUser,
  readUser,
  updateUser,
} from "../controller/userController.js";
// import {
//   createUser,
//   deleteUser,
//   readSpecificUser,
//   readUser,
//   updateUser,
// } from "../controller/userController.js";

export let userRouter = Router();
userRouter.route("/").post(createUser).get(readUser);

userRouter
  .route("/:id")
  .get(readSpecificUser)
  .patch(updateUser)
  .delete(deleteUser);
