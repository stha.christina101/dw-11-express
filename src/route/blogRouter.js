import { Router } from "express";
import { Blog } from "../Schema/model.js";

export let blogRouter = Router();

blogRouter
  .route("/")
  .post(async (req, res, next) => {
    let data = req.body;
    try {
      let result = await Blog.create(data);
      res.json({
        success: true,
        message: "created successfully",
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .get(async (req, res, next) => {
    try {
      let result = await Blog.find({});

      res.json({
        success: true,
        message: "read successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  });
blogRouter
  .route("/:id")
  .get(async (req, res, next) => {
    let id = req.params.id;
    try {
      let result = await Blog.findById(id);

      res.json({
        success: true,
        message: "read successfully 2",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })

  .patch(async (req, res, next) => {
    let id = req.params.id;
    let data = req.body;
    try {
      let result = await Blog.findByIdAndUpdate(id, data, { new: true });
      res.json({
        success: true,
        message: "updated successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .delete(async(req, res, next) => {
    let id = req.params.id;
    try {
      let result = await Blog.findByIdAndDelete(id);
      res.json({
        success: true,
        message: "college deleted successfully",
        data: result,
      });
    } catch (error) {
      res.json({ success: false, message: error.message });
    }
    
  });
