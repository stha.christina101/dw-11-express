import { Router } from "express";

export let traineesRouter = Router();

traineesRouter
  .route("/")
  .post((req, res, next) => {
    console.log(req.query)
    res.json({
        
      success: true,
      message: "trainees created successfully",
    });
  })
  .get((req, res, next) => {
    res.json({
      success: true,
      message: "trainees read successfully",
    });
  })
  .patch((req, res, next) => {
    res.json({
      success: true,
      message: "trainees updated successfully",
    });
  })
  .delete((req, res, next) => {
    res.json({
      success: true,
      message: "trainees deleted successfully",
    });
  });

traineesRouter
  .route("/:id/a/:name") /* dynamic route and they are params*/
  .get((req, res, next) => {
    console.log(req.params);
    res.json({
      success: true,
    });
  })

  .post((req,res,next)=>{
    console.log(req.body)
    console.log(req.params)
    console.log(req.query)
    res.json({success:true}) /* why cant we data?? */

  })