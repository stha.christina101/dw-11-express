import { Router } from "express";
import { Student } from "../Schema/model.js";
import {
  createStudent,
  deleteStudent,
  getSpecificStudent,
  getStudent,
  updateStudent,
} from "../controller/studentController.js";

export let studentsRouter = Router();

studentsRouter
.route("/")
.post(createStudent)
.get(getStudent);


studentsRouter
.route("/:id")
.get(getSpecificStudent)
.patch(updateStudent)
.delete(deleteStudent);
