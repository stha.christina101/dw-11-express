import { model } from "mongoose";
import { studentSchema } from "./studentSchema.js";
import { teacherSchema } from "./teacherSchema.js";
import { traineeSchema } from "./traineeSchema.js";
import { collegeSchema } from "./collegeSchema.js";
import { departmentSchema } from "./departmentSchema.js";
import { classRoomSchema } from "./classRoomSchema.js";
import { blogSchema } from "./blogSchema.js";
import { userSchema } from "./userSchema.js";
// import { reviewRouter } from "../route/reviewRouter.js";
/* singular  */
export let Student = model("Student", studentSchema);
export let Teacher = model("Teacher", teacherSchema);
export let Trainee = model("Trainee", traineeSchema);
export let Department = model("Department", departmentSchema);
export let Classroom = model("Classroom", classRoomSchema);
export let College = model("College", collegeSchema);
export let Blog = model("Blog", blogSchema);
export let User = model("User", userSchema);
// export let Review = model("Review",reviewRouter)
