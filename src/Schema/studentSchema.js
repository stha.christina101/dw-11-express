import { Schema } from "mongoose";

export let studentSchema = Schema({
  name: {
    type: String,
    required: [true, "name field is required"],
    lowercase: true,
    minLength: [3, "name must be at least 3 character long"],
    maxLength: [20, "name must be at most 30 character long"],

    validate: (value) => {
      let onlyAlphabet = /^[a-zA-Z]+$/.test(value);
      if (onlyAlphabet) {
      } else {
        throw new Error("name must be in alphabet");
      }
    },

    // uppercase:true,
    // trim:true,
  },
  phoneNumber: {
    type: Number,
    required: [true, "phoneNumber field is required"],
    trim: true,
    validate: (value) => {
      let strPhoneNumber = String(value);
      if (strPhoneNumber.length === 10) {
      } else {
        throw new Error("phoneNumber must be exact 10 character long");
      }
    },
  },
  gender: {
    type: String,
    default: "male",

    required: [true, "gender field is required"],
    validate: (value) => {
      if (value === "male" || value === "female" || value === "other") {
      } else {
        let error = new Error("gender must be either male,female,other");
        throw error;
      }
    },
  },

  password: {
    type: String,
    required: [true, "password field is required"],
  },
  roll: {
    type: Number,
    required: [true, "roll field is required"],
    min: [50, "roll must be more or equal to 50"],
    max: [100, "roll must be less or equal to 50"],
  },
  isMarried: {
    type: Boolean,
    required: [true, "isMarried field is required"],
  },
  spouseName: {
    type: String,
    required: [true, "spouseName filed is required"],
  },
  email: {
    type: String,
    required: [true, "email field is required"],
    unique: true,
  },
  dob: {
    type: Date,
    required: [true, "DOB field is required"],
  },
  location: {
    country: { type: String, required: [true, "country field is required"] },

    exactLocation: {
      type: String,
      required: [true, "exactLocation field is required"],
    },
  },

  favTeacher: [
    {
      type: String,
    },
  ],
  favSubject: [
    {
      bookName: {
        type: String,
      },
      bookAuthor: {
        type: String,
      },
    },
  ],
});
