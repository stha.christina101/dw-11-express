import { Schema } from "mongoose";

/*   productid
reviewid
descripsion*/
export let reviewSchema = Schema({
    productID:{
        type: Schema.ObjectId,
        ref:"Product",
        required: [true, "productID field is required"],
    },
    userID: {
        type:Schema.ObjectId,
        ref:"User",
        required: [true, "userID field is required"],
    },
    description: {
        type:String,
        required: [true, "description field is required"],
    },
    
  
})
