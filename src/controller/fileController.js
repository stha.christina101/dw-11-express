export let uploadSingleFile = (req, res, next) => {
  console.log(req.file);
  res.json({
    success: true,
    message: "file uploaded successfully",
    link: `http://localhost:8000/${req.file.filename}`,
  });
};
export let uploadMultipleFiles = (req, res, next) => {
  res.json({
    success: true,
    message: "files uploaded succesfully",
  });
};
