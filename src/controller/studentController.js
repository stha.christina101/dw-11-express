import { Student } from "../Schema/model.js";
import { studentsRouter } from "../route/studentRouter.js";

export let createStudent = async (req, res, next) => {
  let data = req.body;
  try {
    let result = await Student.create(data);
    res.json({
      success: true,
      message: "created successfully",
      result:result
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let getStudent = async (req, res, next) => {
  try {
    let result = await Student.find({});

    res.json({
      success: true,
      message: "read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: "unable to read successfully",
    });
  }
};

export let getSpecificStudent = async (req, res, next) => {
  let id = req.params.id;
  try {
    let result = await Student.findById(id);

    res.json({
      success: true,
      message: "read successfully 2",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let updateStudent = async (req, res, next) => {
  let id = req.params.id;
  let data = req.body;
  try {
    let result = await Student.findByIdAndUpdate(id, data, { new: true });
    res.json({
      success: true,
      message: "updated successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteStudent = async (req, res, next) => {
  let id = req.params.id;
  try {
    let result = await Student.findByIdAndDelete(id);
    if (result === null) {
      res.json({
        success: false,
        message: "student doesn't exist",
      });
    } else {
      res.json({
        success: true,
        message: "student deleted successfully",
        data: result,
      });
    }
  } catch (error) {
    res.json({ success: false, message: error.message });
  }
};
