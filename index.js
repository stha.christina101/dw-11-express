import bcrypt from "bcrypt";
import express, { json } from "express";
import { connectToMongodb } from "./src/connectToDb/connectToMongodb.js";
import { blogRouter } from "./src/route/blogRouter.js";
import { firstRouter } from "./src/route/firstRouter.js";
import { studentsRouter } from "./src/route/studentRouter.js";
import { traineesRouter } from "./src/route/traineesRouter.js";
import { userRouter } from "./src/route/userRouter.js";
import { fileRouter } from "./src/route/fileRouter.js";

// import { userRouter } from "./src/route/userRouter.js";
// import { userRouter } from "./src/route/userRouter.js";
let expressApp = express();

expressApp.use(express.static("./public"));

expressApp.use(json());
connectToMongodb();
/* so that json file is received from postman */
// expressApp.use((req, res, next) => {
//   console.log("i am application middleware");
//   next();
// });

expressApp.use("/first", firstRouter);
expressApp.use("/trainees", traineesRouter);
expressApp.use("/students", studentsRouter);
expressApp.use("/blogs", blogRouter);
expressApp.use("/user", userRouter);
expressApp.use("/files", fileRouter);

expressApp.listen(8000, () => {
  console.log("application is listening at post 8000");
});

let hashPassword =
  "$2b$10$flEjWIOZ/5Q6Yfz749Q4quLt7QQuQmDDbdSvRLy8tv2rXD2nDlk9O";
let password = "abc@1234";
let isPasswordMatch = await bcrypt.compare(password, hashPassword);
console.log(isPasswordMatch);

// let password = "abc@1234";
// let hashPassword = await bcrypt.hash(password, 10);
// console.log(hashPassword);
